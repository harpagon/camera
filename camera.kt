class CamVec(x: Float, y: Float) {
	var x = x
	var y = y

	override fun toString(): String {
		return "($x, $y)"
	}
}

class Camera(ws_w: Float, ws_h: Float, vr_w: Float, vr_h: Float) {
	val wsize  = CamVec(ws_w, ws_h) // actual window size
	val vratio = CamVec(vr_w, vr_h) // expected viewport ratio
	val scale  = CamVec(1f, 1f)

	fun get_viewport_size(): CamVec {
		val wratio = wsize.x / wsize.y
		val vratio = vratio.x / vratio.y

		val result = if(wratio >= vratio) {
			CamVec(wsize.y * vratio, wsize.y)
		} else {
			CamVec(wsize.x, wsize.x / vratio)
		}
		result.x *= scale.x
		result.y *= scale.y

		return result;
	}

	fun get_center_offset(vsize: CamVec): CamVec {
		return CamVec(
			(wsize.x - vsize.x) / 2,
			(wsize.y - vsize.y) / 2)
	}
}

fun main() {
	val c = Camera(640f, 480f, 16f, 9f)
	val vsize = c.get_viewport_size()
	val offset = c.get_center_offset(vsize)
	println("vsize = $vsize\noffset = $offset\n")
}

