pub mod camera {

	#[derive(Clone, Copy)]
	pub struct F32Pair {
		pub x: f32,
		pub y: f32,
	}

	impl F32Pair {
		pub fn ratio(&self) -> f32 {
			self.x / self.y
		}
	}

	impl std::fmt::Display for F32Pair {
		fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
			write!(f, "{} x {}", self.x, self.y)
		}
	}
	
	pub struct Camera {
		pub wsize:  F32Pair,
		pub vratio: F32Pair,
		pub scale:  F32Pair,
	}

	impl Camera {
		pub fn get_viewport_size(&self) -> F32Pair {
			let wratio = self.wsize.ratio();
			let vratio = self.vratio.ratio();

			let mut result = if wratio > vratio {
				F32Pair {
				    x: self.wsize.y * vratio,
				    y: self.wsize.y,
				}
			} else {
				F32Pair {
				    x: self.wsize.x,
				    y: self.wsize.x / vratio,
				}
			};
			result.x *= self.scale.x;
			result.y *= self.scale.y;

			return result;
		}

		pub fn get_center_offset(&self, vsize: F32Pair) -> F32Pair {
			F32Pair {
			    x: (self.wsize.x - vsize.x) / 2f32,
			    y: (self.wsize.y - vsize.y) / 2f32,
			}
		}
	}
}

fn main() {
    let c = camera::Camera {
        wsize:  camera::F32Pair {x: 640f32, y: 480f32},
        vratio: camera::F32Pair {x:  16f32, y:   9f32},
        scale:  camera::F32Pair {x:   1f32, y:   1f32},
    };
    let vsize  = c.get_viewport_size();
    let offset = c.get_center_offset(vsize);
    println!("vsize = {}\noffset = {}", vsize, offset);
}

