class Vec:
	__slots__ = ["x", "y"]
	def __init__(self, x, y):
		self.x = x
		self.y = y
	def tuptup(self): #:D
		return (self.x, self.y)
	def __str__(self):
		return f"({self.x}, {self.y})"

class Camera:
	__slots__ = ["wsize", "vratio", "scale"]
	def __init__(self, wsizex, wsizey, vratiox, vratioy):
		self.wsize  = Vec(wsizex, wsizey)
		self.vratio = Vec(vratiox, vratioy)
		self.scale  = Vec(1, 1)
	def get_viewport_size(self):
		wratio = self.wsize.x / self.wsize.y
		vratio = self.vratio.x / self.vratio.y
		result = \
			Vec(self.wsize.y * vratio, self.wsize.y) \
			if wratio > vratio else \
			Vec(self.wsize.x, self.wsize.x / vratio)
		result.x *= self.scale.x
		result.y *= self.scale.y
		return result
	def get_center_offset(self, vsize):
		return Vec(
			(self.wsize.x - vsize.x) / 2,
			(self.wsize.y - vsize.y) / 2)

if __name__ == "__main__":
	c = Camera(640, 480, 16, 9)
	vsize = c.get_viewport_size()
	offset = c.get_center_offset(vsize)
	print(f"vsize = {vsize}\noffset = {offset}")
else:
	from sys import modules
	modules[__name__] = Camera

