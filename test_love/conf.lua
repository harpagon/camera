-- https://love2d.org/wiki/Config_Files

function love.conf(t)
	t.window.title = "Camera module test"
	t.window.width = 800
	t.window.height = 600
	t.window.resizable = true

    t.modules.audio = false
    t.modules.data = false
    t.modules.font = false
    t.modules.image = false
    t.modules.math = false
    t.modules.mouse = false
    t.modules.physics = false
    t.modules.sound = false
    t.modules.system = false
    t.modules.thread = false
    t.modules.timer = false
    t.modules.touch = false
    t.modules.video = false
end

