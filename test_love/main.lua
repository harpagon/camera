local Camera = require 'camera'
local Vec = Camera.Vec

local camera
local keep_centered = false

local function cnv(x, y)
	local result = camera.viewport_size
	return result.x * x / 100, result.y * y / 100
end

local function format_ratio(r)
	return string.format("%d x %d", r.x, r.y)
end

local function debug()
	print(string.format(
		"window_size: %s\nviewport_size: %s\nratio: %s\nscale: %s\noffset: %s\n",
		format_ratio(camera.window_size),
		format_ratio(camera.viewport_size),
		format_ratio(camera.viewport_ratio),
		string.format("%f x %f", camera.scale.x, camera.scale.y),
		format_ratio(camera.offset)
	))
end

local function change_param(param, key, val)
	param[key] = param[key] + val
	love.resize()
end

function love.draw()
	love.graphics.setColor(1, 0.2, 0, 1)
	local w, h = cnv(90, 90)
	local x, y = cnv(5, 5)
	x = x + camera.offset.x
	y = y + camera.offset.y
	love.graphics.rectangle("fill", x, y, w, h)
end

function love.load()
	love.graphics.setBackgroundColor(1, 1, 1)

	camera = Camera:new(Vec(16, 9), Vec(0, 0), Vec(1, 1))
	love.resize()
end

function love.keypressed(key, scancode, isrepeat)
	if key == '-' then
		change_param(camera.viewport_ratio, "x", -1)

	elseif key == '=' then
		change_param(camera.viewport_ratio, "x", 1)

	elseif key == '[' then
		change_param(camera.viewport_ratio, "y", -1)

	elseif key == ']' then
		change_param(camera.viewport_ratio, "y", 1)

	elseif key == ';' then
		change_param(camera.scale, "x", -0.1)

	elseif key == '\'' then
		change_param(camera.scale, "x", 0.1)

	elseif key == ',' then
		change_param(camera.scale, "y", -0.1)

	elseif key == '.' then
		change_param(camera.scale, "y", 0.1)

	elseif key == '1' then
		keep_centered = false
		change_param(camera.offset, "x", -camera.window_size.x / 10)

	elseif key == '2' then
		keep_centered = false
		change_param(camera.offset, "x", camera.window_size.x / 10)

	elseif key == '3' then
		keep_centered = false
		change_param(camera.offset, "y", -camera.window_size.y / 10)

	elseif key == '4' then
		keep_centered = false
		change_param(camera.offset, "y", camera.window_size.y / 10)

	elseif key == 'c' then
		keep_centered = not keep_centered
		camera:center()
		debug()

	elseif key == 'z' then
		keep_centered = false
		camera.offset.x = 0
		camera.offset.y = 0
		love.resize()
		debug()

	elseif key == 'f5' then
		love.window.setFullscreen(true)

	elseif key == 'f6' then
		love.window.setFullscreen(false)

	elseif key == 'escape' then
		love.event.quit()

	end
end

function love.resize()
	local window_size = Vec(love.graphics.getDimensions())
	camera:set_viewport_size(window_size)
	if keep_centered then
		camera:center()
	end
	debug()
end

