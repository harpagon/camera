local Camera = {}
Camera.__index = Camera

local function ratio_to_float(r)
	return r.x / r.y
end

function Camera:new(viewport_ratio, offset, scale)
	local result = {
		viewport_ratio=viewport_ratio,
		offset=offset,
		scale=scale
	}
	return setmetatable(result, Camera)
end

function Camera:set_viewport_size(window_size)
	local window_ratio = ratio_to_float(window_size)
	local viewport_ratio = ratio_to_float(self.viewport_ratio)
	local scale = self.scale
	local w, h

	if window_ratio > viewport_ratio then
		w = window_size.y * viewport_ratio
		h = window_size.y
	else
		w = window_size.x
		h = window_size.x / viewport_ratio
	end

	if scale ~= nil then
		w = w * scale.x
		h = h * scale.y
	end

	self.window_size = window_size
	self.viewport_size = Camera.Vec(w, h)
end

function Camera:center()
	local x = (self.window_size.x - self.viewport_size.x) / 2
	local y = (self.window_size.y - self.viewport_size.y) / 2
	self.offset = Camera.Vec(x, y)
end

function Camera.Vec(x, y)
	return {x=x, y=y}
end

return Camera

