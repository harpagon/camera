#ifndef __CAMERA_H__
#define __CAMERA_H__

typedef struct {
	float x;
	float y;
} CamVec;

typedef struct {
	CamVec wsize;  // actual window size
	CamVec vratio; // expected viewport ratio
	CamVec scale;
} Camera;

CamVec camera_get_viewport_size(Camera *camera);

// vsize - viewport size
CamVec camera_get_center_offset(Camera *camera, CamVec vsize);

#endif//__CAMERA_H__


#ifdef __CAMERA_IMPL__

CamVec
camera_get_viewport_size(Camera *c)
{
	CamVec result;
	float wratio = c->wsize.x / c->wsize.y;
	// optimization: store float vratio in Camera
	// instead of CamVec vratio
	float vratio = c->vratio.x / c->vratio.y;

	if(wratio >= vratio) {
		result.x = c->wsize.y * vratio;
		result.y = c->wsize.y;
	} else {
		result.x = c->wsize.x;
		result.y = c->wsize.x / vratio;
	}

	result.x *= c->scale.x;
	result.y *= c->scale.y;

	return result;
}

CamVec
camera_get_center_offset(Camera *c, CamVec vsize)
{
	CamVec offset = {
		(c->wsize.x - vsize.x) / 2,
		(c->wsize.y - vsize.y) / 2,
	};
	return offset;
}

#endif//__CAMERA_IMPL__


#ifdef __CAMERA_EXAMPLE__

int main()
{
	Camera c = {
		.wsize  = { .x = 640, .y = 480 },
		.vratio = { .x =  16, .y =   9 },
		.scale  = { .x =   1, .y =   1 },
	};
	CamVec vsize = camera_get_viewport_size(&c);
	CamVec offset = camera_get_center_offset(&c, vsize);
	printf(
			"vsize = (%f, %f)\noffset = (%f, %f)\n",
			vsize.x, vsize.y,
			offset.x, offset.y);
}

#endif//__CAMERA_EXAMPLE__

